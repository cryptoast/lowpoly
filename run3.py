import imgutils as img
import svgutils as svg
import tricolor as tri
import points as pts
from sys import argv
import cv2

def main():
    FILEROOT = argv[1]
    INDEX = int(argv[2])

    orig = cv2.imread('./src/' + FILEROOT + '.png')
    dims = orig.shape

    triangles = pts.get_triangles('./tmp/{}.1.node'.format(FILEROOT),'./tmp/{}.1.ele'.format(FILEROOT))

    origcolors = tri.get_tricolors(triangles,orig)

    while True:
        try:
            red = int(input('red: '))
            green = int(input('green: '))
            blue = int(input('blue: '))
            rbias = float(input('rbias: '))
            gbias = float(input('gbias: '))
            bbias = float(input('bbias: '))
            dom = float(input('dominance: '))


            colorsrc = img.biased_image(dims[0],dims[1],
                                        red,green,blue,
                                        rbias=rbias,gbias=gbias,bbias=bbias)
            randcolors = tri.get_tricolors(triangles,colorsrc)

            tricolors = tri.blend_tricolors(randcolors,origcolors,wt1=dom,wt2=1-dom)

            fout = './out/{0:s}_color/{0:s}_clr{1:d}.svg'.format(FILEROOT,INDEX)

            svg.create_svg(tricolors,outfile=fout)

            print('saved at ' + fout)

            INDEX += 1
        except (ValueError,KeyboardInterrupt) as e:
            break



if __name__=='__main__':
    main()
