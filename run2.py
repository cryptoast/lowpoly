import svgutils as svg
import points as pts
import tricolor as tri
from sys import argv
import cv2


def main():
    FILEROOT = argv[1]

    img = cv2.imread('./src/' + FILEROOT + '.png',cv2.IMREAD_UNCHANGED)

    triangles = pts.get_triangles('./tmp/' + FILEROOT + '.1.node','./tmp/' + FILEROOT + '.1.ele')

    tricolors = tri.get_tricolors(triangles,img)
    tricolors = [t for t in tricolors if t != None]

    svg.create_svg(tricolors,outfile='./out/' + FILEROOT + '.svg')

    return


if __name__=='__main__':
    main()
