#!/bin/bash

mkdir -p tmp out

# $1 is root of filename

# Generate points from image
# python3 convulsion.py $1

# Generate .node file from image
python3 run1.py $1


# Generate Triangles from .node file
./triangle ./tmp/$1.node


# Get Average Color per Triangle
# Create svg with Colored Triangles
python3 run2.py $1

# Prep for Hue-Biased SVGs
mkdir -p out/$1_color
read COUNT <<< $(ls out/$1_color/* | wc -w)

python3 run3.py $1 $COUNT

# clean up tmp
rm ./tmp/*.node ./tmp/*.ele
