import points as pts
from sys import argv
import cv2

def main():
    FILEROOT = argv[1]

    #img = cv2.imread(FILEROOT + '.png')
    #imgpoints = cv2.imread(FILEROOT + '_pts.png')

    pts.node_file_from_bitmap(infile='./src/pts/' + FILEROOT + '_pts.png',outfile='./tmp/' + FILEROOT + '.node')

    return

if __name__=='__main__':
    main()
