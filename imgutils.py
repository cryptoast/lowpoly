import cv2, random, math
import numpy as np

def random_px():
    return np.ndarray([random.randint(0,255),random.randint(0,255),random.randint(0,255)])

def biased_px(red,green,blue,rbias=.5,gbias=.5,bbias=.5):
    rrand = random.randint(0,255)
    rnew = int(math.floor(red + (rrand - red) * (1-rbias)))

    brand = random.randint(0,255)
    bnew = int(math.floor(blue + (brand - blue) * (1-bbias)))

    grand = random.randint(0,255)
    gnew = int(math.floor(green + (grand - green) * (1-gbias)))

    return (bnew,gnew,rnew)

def random_image(height,width):
    img = np.zeros((height,width,3),dtype=np.uint8)
    for i in range(height):
        for j in range(width):
            img[i,j] = random_px()

    return img

def biased_image(height,width,red,green,blue,rbias=.5,gbias=.5,bbias=.5):
    img = np.zeros((height,width,3),dtype=np.uint8)
    for i in range(height):
        for j in range(width):
            img[i,j] = biased_px(red,green,blue,rbias=rbias,gbias=gbias,bbias=bbias)

    return img
