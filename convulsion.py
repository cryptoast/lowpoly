import cv2
from sys import argv

def do_stuff(infile,outfile,lothresh,hithresh):
    img = cv2.imread(infile,0)
    edges = cv2.Canny(img,lothresh,hithresh)
    dims = img.shape
    for row in range(dims[0]):
        for col in range(dims[1]):
            if edges[row,col] == 255:
                edges[row,col] = 0
            else:
                edges[row,col] = 255
    cv2.imwrite(outfile,edges)

def main():
    FILEROOT = argv[1]
    
    if len(argv) >= 4:
        lothresh,hithresh = int(argv[2]),int(argv[3])
    else:
        lothresh = 50
        hithresh = 150

    do_stuff(FILEROOT + '.png',FILEROOT + '_pts.png',lothresh,hithresh)


if __name__ == '__main__':
    main()
