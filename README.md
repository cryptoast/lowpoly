# LOWPOLY TOOLKIT
Developed by Jesse Martinez (cryptoast) 2019

## About the Toolkit
LOWPOLY is a toolkit designed to render vectorized, low-poly triangle meshes from input images, and enable artistic manipulations of such renderings.

The toolkit will (eventually) accomplish the following tasks:

* Produce a dotmap from the color gradients of a source image (in progress)
* Produce a Delaunay Triangulation from a dotmap (done! using Jonathan Richard Shewchuk's "Triangle" project; check the README in ./Triangle for more info!)
* Produce a colored SVG of the Triangulation, with colors matching the average color within each triangle (done!)
* Enable "color biasing" of the colored SVG, randomly hueing the SVG given particular RGB parameters & biases (functional, but will be improved)
