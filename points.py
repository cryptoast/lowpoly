import cv2, random, re
import numpy as np

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y

class Segment:
    def __init__(self,a,b):
        if a != b:
            self.a = a
            self.b = b
        else:
            raise ValueError

    def slope(self):
        if (self.a.x == self.b.x):
            return None
        else:
            return (self.b.y - self.a.y)/(self.b.x - self.a.x)

    def xr(self):
        return (min([self.a.x,self.b.x]),max([self.a.x,self.b.x]))

    def yr(self):
        return (min([self.a.y,self.b.y]),max([self.a.y,self.b.y]))

    def yint(self):
        return self.a.y - self.slope() * self.a.x

    def xgety(self,x):
        return self.yint() + self.slope() * x

    def ix(self,l2):
        a1,b1,a2,b2 = self.a,self.b,l2.a,l2.b

        if a1 == a2 or a1 == b2 or b1 == a2 or b1 == b2:
            return True

        xr1,xr2 = self.xr(), l2.xr()
        yr1,yr2 = self.yr(), l2.yr()

        if (xr1[0] > xr2[1]) or (xr2[0] > xr1[1]) or (yr1[0] > yr2[1]) or (yr2[0] > yr1[1]):
            return False

        m1,m2 = self.slope(),l2.slope()
        if m1 == m2:
            return False

        if m1 == None:
            yy = l2.xgety(self.a.x)
            if yr1[0] <= yy <= yr1[1]:
                return True
            else:
                return False
        elif m2 == None:
            yy = self.xgety(l2.a.x)
            if (yr2[0] <= yy <= yr2[1]):
                return True
            else:
                return False

        xa = (l2.yint() - self.yint()) / (m1 - m2)

        if xa < max([xr1[0],xr2[0]]) or xa > min([xr1[1],xr2[1]]):
            return False
        else:
            return True

def generate_test_img(width=64,height=64,points=200,outfile='rand.png'):
    img = np.full((height,width,3),255)
    for _ in range(points):
        row,col = random.randint(0,height-1),random.randint(0,width-1)
        img[row,col] = [0,0,0]

    cv2.imwrite(outfile,img)
    return


def node_file_from_bitmap(infile=None,outfile=None):
    if infile == None:
        infile = input('infile: ')

    img = cv2.imread(infile,0)

    nodes = []

    dims = img.shape
    for row in range(dims[0]):
        for col in range(dims[1]):
            if img[row,col] == 0:
                nodes.append((col,-1*row)) # x-coor is col, y-coor is row

    if outfile == None:
        outfile = input('outfile: ')

    y_offset = abs(min([y for x,y in nodes]))
    nodes = [(x,y + y_offset) for x,y in nodes]

    with open(outfile,'w') as fout:
        fout.write(str(len(nodes)) + ' 2 0 0\n')
        for i,nd in enumerate(nodes):
            fout.write('{0:d} {1:d} {2:d}\n'.format(i,nd[0],nd[1]))

    return

def points_from_node_file(infile):
    points = []
    with open(infile) as f:
        f.readline()
        for l in f.readlines():
            _,x,y = l.split(' ')
            points.append(Point(int(x),int(y)))
    return points

def get_triangles(nodefile,elefile):
    regexp = re.compile(r'\S+')
    with open(nodefile) as f:
        f.readline()
        points = []
        for l in f.readlines():
            if l[0] == '#':
                continue
            m = regexp.findall(l)
            idx, xcoor, ycoor = m[0],m[1],m[2]
            points.append(Point(int(xcoor),int(ycoor)))

    with open(elefile) as f:
        f.readline()
        triangles = []
        for l in f.readlines():
            if l[0] == '#':
                continue
            m = regexp.findall(l)
            idx, v1, v2, v3 = m[0],int(m[1]),int(m[2]),int(m[3])
            triangles.append((points[v1],points[v2],points[v3]))

    return triangles
