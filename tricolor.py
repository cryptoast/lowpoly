import cv2, random, math
from points import *

def color_avg(colors):
    ncolors = len(colors)
    try:
        if (len([c for c in colors if c[3] != 0]) < ncolors / 2):
            return None
    except IndexError:
        pass

    #print(colors[1])
    return (sum([c[0] for c in colors])//ncolors,
            sum([c[1] for c in colors])//ncolors,
            sum([c[2] for c in colors])//ncolors)

def weighted_color_avg(c1,c2,w1,w2):
    if c1 == None or c2 == None:
        return None

    blue = int(math.floor(c1[0]*w1 + c2[0]*w2))
    green = int(math.floor(c1[1]*w1 + c2[1]*w2))
    red = int(math.floor(c1[2]*w1 + c2[2]*w2))

    return (blue,green,red)

def get_sign(v1,v2,v3):
    return (v1.x - v3.x) * (v2.y - v3.y) - (v2.x - v3.x) * (v1.y - v3.y)

def is_point_in_triangle(pt,a,b,c):
    d1 = get_sign(pt,a,b)
    d2 = get_sign(pt,b,c)
    d3 = get_sign(pt,c,a)

    has_neg = (d1 < 0) or (d2 < 0) or (d3 < 0)
    has_pos = (d1 > 0) or (d2 > 0) or (d3 > 0)

    return (not (has_neg and has_pos))

def get_points_in_triangle(v1,v2,v3,numpts = 10):
    minx = min([v1.x,v2.x,v3.x])
    maxx = max([v1.x,v2.x,v3.x])
    miny = min([v1.y,v2.y,v3.y])
    maxy = max([v1.y,v2.y,v3.y])

    out = []
    for _ in range(numpts):
        testpt = Point(random.randint(minx,maxx),random.randint(miny,maxy))
        while not is_point_in_triangle(testpt,v1,v2,v3):
            testpt = Point(random.randint(minx,maxx),random.randint(miny,maxy))
        
        out.append(testpt)

    return out

def get_colors_from_points(img,points):
    #print(img[0][0])
    return [img[p.y * -1][p.x] for p in points]

def triangle_color(img,v1,v2,v3):
    pts = get_points_in_triangle(v1,v2,v3)
    colors = get_colors_from_points(img,pts)
    return color_avg(colors)

def tricolor(v1,v2,v3,img):
    color = triangle_color(img,v1,v2,v3)
    if color == None:
        return None
    return (((v1.x,v1.y),(v2.x,v2.y),(v3.x,v3.y)),color)

def get_tricolors(tris,img):
    return [tricolor(t[0],t[1],t[2],img) for t in tris]

# BUG: transparent tris cut from src img, not from color img
def blend_tricolors(tris1,tris2,wt1=.5,wt2=.5):
    newcolors = [weighted_color_avg(t1[1],t2[1],wt1,wt2) for t1,t2 in zip(tris1,tris2)]
    return [(t[0],n) for t,n in zip(tris1,newcolors) if n != None]
