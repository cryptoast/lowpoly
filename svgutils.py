# svgutils.py

# assumes input in range [0,255]
def num_to_hex(num):
    x = num // 16
    y = num % 16

    strx = '0123456789abcdef'[x]
    stry = '0123456789abcdef'[y]

    return strx + stry

def bgr_to_hex(bgr):
    rhex = num_to_hex(bgr[2])
    ghex = num_to_hex(bgr[1])
    bhex = num_to_hex(bgr[0])

    return r"#" + rhex + ghex + bhex

def class_string(bgr,idx):
    hx = bgr_to_hex(bgr)
    return (".cls-%d{fill:%s;}" % (idx,hx))
    #return (".cls-%d{fill:%s;stroke:%s;stroke-width:1;}" % (idx,hx,hx))

def triangle_string(pt1,pt2,pt3,idx):
    x1,y1 = pt1
    x2,y2 = pt2
    x3,y3 = pt3
    return ("<polygon class=\"cls-{0:d}\" points=\"{1:.2f} {2:.2f} {3:.2f} {4:.2f} {5:.2f} {6:.2f} {1:.2f} {2:.2f}\" />".format(idx,x1,y1,x2,y2,x3,y3))


def create_svg(tricolors,outfile=None):
    if outfile == None:
        outfile = input('outfile: ')

    xmax = max([max([pt[0] for pt in tri]) for tri,_ in tricolors])
    xmin = min([min([pt[0] for pt in tri]) for tri,_ in tricolors])
    ymax = max([max([pt[1] for pt in tri]) for tri,_ in tricolors])
    ymin = min([min([pt[1] for pt in tri]) for tri,_ in tricolors])

    #y_offset = abs(ymin)

    with open(outfile,'w') as f:

        # open svg
        f.write('<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox = "{0:.2f} {1:.2f} {2:.2f} {3:.2f}">'.format(xmin,ymin,xmax,ymax))

        # open class defs
        f.write('<defs><style>')

        # write class defs
        for i,tricolor in enumerate(tricolors):
            _,color = tricolor
            f.write(class_string(color,i))
        # close class defs
        f.write('</style></defs>')

        f.write('<title>test_file</title>')

        # write polygon defs
        for i,tricolor in enumerate(tricolors):
            pts,_ = tricolor
            pts = [(pt[0],ymax - pt[1]) for pt in pts]
            f.write(triangle_string(pts[0],pts[1],pts[2],i))

        # close svg
        f.write('</svg>')

    return
